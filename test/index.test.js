const { describe } = require('tape-plus')
const hypercore = require('hypercore')
const path = require('path')
const fs = require('fs')
const sodium = require('sodium-native')

const crypto = require('../')

const { cleanup, tmp } = require('./util')

describe('key generation', (context) => {
  context('masterKey()', (assert, next) => {
    const masterKey = crypto.masterKey()
    assert.ok(masterKey, 'key generated successfully')
    next()
  })

  context('keyPair()', (assert, next) => {
    const masterKey = crypto.masterKey()
    const keypair = crypto.keyPair(masterKey, 0)
    assert.ok(keypair, 'keys successfully generated')
    assert.ok(keypair.publicKey instanceof Buffer, 'public key is a buffer')
    assert.ok(keypair.secretKey instanceof Buffer, 'secret key is a buffer')
    next()
  })

  context('encryptionKey()', (assert, next) => {
    const key = crypto.encryptionKey()
    assert.ok(key, 'Key successfully generated')
    assert.ok(key instanceof Buffer, 'key is a secure buffer')
    next()
  })

  context('encryptionKey(string)', (assert, next) => {
    const bytes = crypto.randomBytes(32).toString('hex')
    const key = crypto.encryptionKey(bytes)
    assert.ok(key, 'Key successfully generated')
    assert.ok(key instanceof Buffer, 'key is a secure buffer')
    assert.same(bytes, key.toString('hex'), 'keys match')
    next()
  })

  context('encryptionKey(buffer)', (assert, next) => {
    const bytes = crypto.randomBytes(32)
    const encKeyStr = bytes.toString('hex')
    const key = crypto.encryptionKey(bytes)
    assert.ok(key, 'Key successfully generated')
    assert.ok(key instanceof Buffer, 'key is a secure buffer')
    assert.same(encKeyStr, key.toString('hex'), 'keys match')
    next()
  })

  context('accessKey()', (assert, next) => {
    const accessKey = crypto.accessKey()
    assert.ok(accessKey, 'Key successfully generated')
    assert.same(accessKey.length, 64, 'Read key is 64 bytes')
    assert.ok(accessKey instanceof Buffer, 'Read key is a buffer')
    next()
  })

  context('pack(buffer, buffer)', (assert, next) => {
    const address = crypto.randomBytes(32)
    const encryptionKey = crypto.encryptionKey()

    const accessKey = crypto.pack(address, encryptionKey)
    assert.ok(accessKey, 'Key successfully generated')
    assert.ok(accessKey instanceof Buffer, 'Access key is a buffer')
    assert.same(accessKey.length, 64, 'read key is 64 bytes')
    assert.same(accessKey.slice(0, 32), address)
    assert.same(accessKey.slice(32, 64), encryptionKey)
    next()
  })

  context('pack(string, string)', (assert, next) => {
    const address = crypto.randomBytes(32)
    const secretKey = crypto.encryptionKey()

    const accessKey = crypto.pack(address.toString('hex'), secretKey.toString('hex'))
    assert.ok(accessKey, 'Key successfully generated')
    assert.ok(accessKey instanceof Buffer, 'Access key is a buffer')
    assert.same(accessKey.length, 64, 'read key is 64 bytes')
    assert.same(accessKey.slice(0, 32), address)
    assert.same(accessKey.slice(32, 64), secretKey)
    next()
  })

  context('unpack(buffer)', (assert, next) => {
    const accessKey = crypto.accessKey()

    const keys = crypto.unpack(accessKey)
    const address = accessKey.slice(0, 32)
    const encryptionKey = accessKey.slice(32, 64)

    assert.same(keys.address, address, 'Unpacks the public key')
    assert.same(keys.encryptionKey, encryptionKey, 'Unpacks the symmetric key')
    next()
  })

  context('isKey()', (assert, next) => {
    const accessKey = crypto.accessKey()
    assert.ok(crypto.isKey(accessKey), '64 byte buffer is a valid read/write key')
    assert.ok(crypto.isKey(accessKey.toString('hex')), '64 byte string is a valid read/write key')

    const blindKey = crypto.randomBytes(32)
    assert.ok(crypto.isKey(blindKey), '32 byte public key is a valid blind replication key')
    assert.ok(crypto.isKey(blindKey.toString('hex')), '32 byte string is a valid blind replication key')

    next()
  })

  context('unpack(string)', (assert, next) => {
    const accessKey = crypto.accessKey()

    const keys = crypto.unpack(accessKey.toString('hex'))
    const address = accessKey.slice(0, 32)
    const encryptionKey = accessKey.slice(32, 64)

    assert.same(keys.address, address, 'Unpacks the public key')
    assert.same(keys.encryptionKey, encryptionKey, 'Unpacks the symmetric key')
    next()
  })

  context('mnemnonic', (assert, next) => {
    const encryptionKey = crypto.encryptionKey()
    const words = crypto.keyToMnemonic(encryptionKey)
    assert.ok(words, 'generates a mnemonic')
    assert.ok(words.split(/\s/).length, 24, '24 words long')

    var check = crypto.mnemonicToKey(words)
    assert.same(encryptionKey, check, 'returns the correct key')

    next()
  })

  context('genericHash', (assert, next) => {
    const string = Buffer.from('wolf-person')
    assert.throws(() => crypto.genericHash(string, null, 5), 'Error: invalid length property', 'invalid length')
    assert.throws(() => crypto.genericHash(string, null, 'pirates'), 'Error: invalid length property', 'invalid length')
    assert.throws(() => crypto.genericHash(string, null, 65), 'Error: invalid length property', 'invalid length')

    assert.ok(crypto.genericHash(string, null, 16), 'valid length')
    assert.ok(crypto.genericHash(string, null, 64), 'valid length')

    next()
  })
})

describe('message encoding', (context) => {
  context('encrypt and decrypt a message', (assert, next) => {
    const key = crypto.encryptionKey()
    const nonce = crypto.encoder.generateNonce()
    const encoder = crypto.encoder(key, {
      nonce,
      valueEncoding: 'utf8'
    })

    const encrypted = encoder.encode("Hello World")
    assert.ok(encrypted, 'Encrypts the message')
    assert.ok(encrypted instanceof Buffer, 'Returns a buffer')

    const message = encoder.decode(encrypted)
    assert.same(message, 'Hello World', 'Decrypts the message')
    next()
  })
})

describe('hypercore', (context) => {
  let storage

  context.beforeEach((c) => {
    storage = tmp()
  })

  context('encrypted the log', (assert, next) => {
    const key = crypto.encryptionKey()
    const nonce = crypto.encoder.generateNonce()
    const encoder = crypto.encoder(key, {
      nonce,
      valueEncoding: 'utf8'
    })
    const feed = hypercore(storage, { valueEncoding: encoder })

    feed.append('boop', (err) => {
      assert.error(err, 'no error')

      var data = fs.readFileSync(path.join(storage, 'data'))
      assert.notSame(data, 'boop', 'log entry is encrypted')
      assert.same(encoder.decode(data), 'boop', 'log entry is encrypted')

      feed.get(0, (err, entry) => {
        assert.error(err, 'no error')
        assert.same('boop', entry, 'hypercore decrypts the message')

        cleanup(storage, next)
      })
    })
  })

  context('encrypted the log, with a json object', (assert, next) => {
    const key = crypto.encryptionKey()
    const nonce = crypto.encoder.generateNonce()
    const encoder = crypto.encoder(key, {
      nonce,
      valueEncoding: 'json'
    })
    const feed = hypercore(storage, { valueEncoding: encoder })

    const message = { boop: 'beep' }

    feed.append(message, (err) => {
      assert.error(err, 'no error')

      feed.get(0, (err, entry) => {
        assert.error(err, 'no error')
        assert.same(message, entry, 'hypercore decrypts the message')

        cleanup(storage, next)
      })
    })
  })
})

describe('box and unbox', (context) => {
  context('boxkeypair', (assert, next) => {
    const masterKey = crypto.masterKey()
    const { publicKey, secretKey } = crypto.deriveBoxKeyPair(masterKey, 4)
    assert.ok(Buffer.isBuffer(publicKey), 'public key generated')
    assert.ok(Buffer.isBuffer(secretKey), 'secret key generated')
    const seed = crypto.randomBytes(sodium.crypto_box_SEEDBYTES)
    const seedKeypair = crypto.boxKeyPair(seed)
    assert.ok(Buffer.isBuffer(seedKeypair.publicKey), 'public key generated from seed')
    assert.ok(Buffer.isBuffer(seedKeypair.secretKey), 'secret key generated from seed')
    next()
  })

  context('box and unbox', (assert, next) => {
    const masterKey = crypto.masterKey()
    const { publicKey, secretKey } = crypto.deriveBoxKeyPair(masterKey, 4)
    const msg = 'beep boop'
    const boxedMsg = crypto.box(publicKey, Buffer.from(msg))
    assert.true(boxedMsg.toString('hex') !== msg.toString('hex'), 'boxed message !== message')
    const unboxedMsg = crypto.unbox(boxedMsg, { publicKey, secretKey })
    assert.same(unboxedMsg.toString(), msg, 'message decrypted successfully')
    next()
  })
})
