const sodium = require('sodium-native')
const crypto = require('hypercore-crypto')
const assert = require('assert')
const bip39 = require('bip39')
const encoder = require('@coboxcoop/crypto-encoder')
const zero = sodium.sodium_memzero

const PACKEDLENGTH = sodium.crypto_sign_PUBLICKEYBYTES + sodium.crypto_secretbox_KEYBYTES

module.exports = {
  PACKEDLENGTH,
  randomBytes,
  address,
  encryptionKey,
  masterKey,
  keyPair,
  keySet,
  accessKey,
  pack,
  unpack,
  isKey,
  toBuffer,
  keyToMnemonic: bip39.entropyToMnemonic,
  mnemonicToKey,
  encoder,
  deriveBoxKeyPair,
  boxKeyPair,
  box,
  unbox,
  genericHash,
  scalarMult
}

function randomBytes (length) {
  return crypto.randomBytes(length)
}

function masterKey () {
  const key = sodium.sodium_malloc(sodium.crypto_kdf_KEYBYTES)
  sodium.crypto_kdf_keygen(key)
  return key
}

function address () {
  return randomBytes(sodium.crypto_secretbox_KEYBYTES)
}

function encryptionKey (encryptionKey) {
  if (!encryptionKey) return encoder.encryptionKey()
  var key = sodium.sodium_malloc(sodium.crypto_secretbox_KEYBYTES)

  if (encryptionKey && Buffer.isBuffer(encryptionKey) && !encryptionKey.secure) {
    key.write(encryptionKey.toString('hex'), 'hex')
    zero(encryptionKey)
  } else if (encryptionKey && Buffer.isBuffer(encryptionKey) && encryptionKey.secure) {
    return encryptionKey
  } else if (encryptionKey && typeof encryptionKey === 'string') {
    key.write(encryptionKey, 'hex')
  } else return encoder.encryptionKey()

  return key
}

function deriveSeed (masterKey, id, ctxt = 'coboxcobox') {
  const context = sodium.sodium_malloc(sodium.crypto_hash_sha256_BYTES)
  sodium.crypto_hash_sha256(context, Buffer.from(ctxt))
  const seed = sodium.sodium_malloc(sodium.crypto_kdf_KEYBYTES)

  sodium.crypto_kdf_derive_from_key(seed, id, context, masterKey)
  zero(context)
  return seed
}

function keyPair (masterKey, id, ctxt = 'coboxcobox') {
  return crypto.keyPair(deriveSeed(masterKey, id, ctxt))
}

function keySet () {
  return {
    address: address(),
    encryptionKey: encryptionKey()
  }
}

function accessKey () {
  return pack(randomBytes(sodium.crypto_secretbox_KEYBYTES), encryptionKey())
}

function pack (address, encryptionKey) {
  address = toBuffer(address, sodium.crypto_sign_PUBLICKEYBYTES)
  encryptionKey = toBuffer(encryptionKey, sodium.crypto_secretbox_KEYBYTES)
  const accessKey = sodium.sodium_malloc(PACKEDLENGTH)
  address.copy(accessKey)
  encryptionKey.copy(accessKey, sodium.crypto_secretbox_KEYBYTES)
  return accessKey
}

function unpack (key) {
  key = toBuffer(key, [sodium.crypto_sign_PUBLICKEYBYTES, PACKEDLENGTH])

  if (key.length === sodium.crypto_sign_PUBLICKEYBYTES) return { address: key }

  const address = key.slice(0, sodium.crypto_sign_PUBLICKEYBYTES)
  const encryptionKey = key.slice(sodium.crypto_secretbox_KEYBYTES)

  return {
    address,
    encryptionKey
  }
}

function isKey (key) {
  if (!(key instanceof Buffer || typeof key === 'string')) return false
  const length = Buffer.from(key, 'hex').length
  return length === sodium.crypto_sign_PUBLICKEYBYTES ||
    length === sodium.crypto_sign_PUBLICKEYBYTES + sodium.crypto_secretbox_KEYBYTES
}

function toBuffer (stringOrBuffer, lengths) {
  if (!lengths) lengths = [sodium.crypto_sign_PUBLICKEYBYTES, sodium.crypto_secretbox_KEYBYTES]
  if (typeof lengths === 'number') lengths = [lengths]
  if ((Buffer.isBuffer(stringOrBuffer)) && (lengths.indexOf(stringOrBuffer.length) > -1)) return stringOrBuffer
  assert(typeof stringOrBuffer === 'string', 'Key is incorrect type')
  const res = Buffer.from(stringOrBuffer, 'hex')
  assert(lengths.indexOf(res.length) > -1, 'Invalid key')
  return res
}

function mnemonicToKey (mnemonic) {
  return toBuffer(bip39.mnemonicToEntropy(mnemonic), [sodium.crypto_secretbox_KEYBYTES])
}

function deriveBoxKeyPair (masterKey, id, ctxt = 'coboxcobox') {
  return boxKeyPair(deriveSeed(masterKey, id, ctxt))
}

function boxKeyPair (seed) {
  const publicKey = sodium.sodium_malloc(sodium.crypto_box_PUBLICKEYBYTES)
  const secretKey = sodium.sodium_malloc(sodium.crypto_box_SECRETKEYBYTES)
  if (seed) {
    seed = toBuffer(seed, sodium.crypto_box_SEEDBYTES)
    sodium.crypto_box_seed_keypair(publicKey, secretKey, seed)
    zero(seed)
  } else {
    sodium.crypto_box_keypair(publicKey, secretKey)
  }
  return { publicKey, secretKey }
}

function box (pubKey, messageBuffer, contextMessage = Buffer.from('cobox')) {
  pubKey = toBuffer(pubKey, sodium.crypto_box_PUBLICKEYBYTES)
  if (typeof messageBuffer === 'string') messageBuffer = Buffer.from(messageBuffer)
  if (typeof contextMessage === 'string') contextMessage = Buffer.from(contextMessage)
  var boxed = Buffer.alloc(messageBuffer.length + sodium.crypto_secretbox_MACBYTES)
  const ephKeypair = boxKeyPair()
  const nonce = randomBytes(sodium.crypto_secretbox_NONCEBYTES)
  var sharedSecret = genericHash(
    Buffer.concat([ephKeypair.publicKey, pubKey, contextMessage]),
    genericHash(scalarMult(ephKeypair.secretKey, pubKey)))

  sodium.crypto_secretbox_easy(boxed, messageBuffer, nonce, sharedSecret)

  zero(sharedSecret)
  zero(ephKeypair.secretKey)
  return Buffer.concat([nonce, ephKeypair.publicKey, boxed])
}

function unbox (cipherText, keypair, contextMessage = Buffer.from('cobox')) {
  keypair.publicKey = toBuffer(keypair.publicKey, sodium.crypto_box_PUBLICKEYBYTES)
  keypair.secretKey = toBuffer(keypair.secretKey, sodium.crypto_box_SECRETKEYBYTES)
  if (typeof contextMessage === 'string') contextMessage = Buffer.from(contextMessage)
  const NONCEBYTES = sodium.crypto_secretbox_NONCEBYTES
  const KEYBYTES = sodium.crypto_secretbox_KEYBYTES
  try {
    var nonce = cipherText.slice(0, NONCEBYTES)
    var pubKey = cipherText.slice(NONCEBYTES, NONCEBYTES + KEYBYTES)
    var box = cipherText.slice(NONCEBYTES + KEYBYTES, cipherText.length)
    var unboxed = Buffer.alloc(box.length - sodium.crypto_secretbox_MACBYTES)
  } catch (err) {
    return false
  }
  const sharedSecret = genericHash(
    Buffer.concat([pubKey, keypair.publicKey, contextMessage]),
    genericHash(scalarMult(keypair.secretKey, pubKey)))

  const success = sodium.crypto_secretbox_open_easy(unboxed, box, nonce, sharedSecret)
  zero(sharedSecret)
  zero(keypair.secretKey)
  zero(keypair.publicKey)
  return success ? unboxed : false
}

function genericHash (msg, key, length = sodium.crypto_generichash_BYTES_MAX) {
  assert(
    typeof length === 'number' &&
    length >= sodium.crypto_generichash_BYTES_MIN &&
    length <= sodium.crypto_generichash_BYTES_MAX,
    'invalid length property'
  )
  var hash = sodium.sodium_malloc(length)
  sodium.crypto_generichash(hash, msg, key)
  return hash
}

function scalarMult (sk, pk) {
  var result = sodium.sodium_malloc(sodium.crypto_scalarmult_BYTES)
  sodium.crypto_scalarmult(result, sk, pk)
  return result
}
